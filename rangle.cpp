#include <iostream>
#include <string>
#include <stdio.h>
#include <deque>
#include <queue>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <vector>

#define PI 3.14159265358979323846
#define   TRUE    1
#define   FALSE   0

#define BEACONDIST 180

#define MAXX 2*BEACONDIST
#define MINX 0
#define MAXY 2*BEACONDIST
#define MINY 0



using namespace std;

double angle_one(double x, double y) { //angle between beacon 1 and 2
  double a1;
  double a2;
  if(x>180) {
    a1 = atan(((double)(x-180))/(double)y);
    a2 = atan(((double)x)/(double)y);
    return a2-a1;
  }
  else if(x<180) {
    a1 = atan(((double)(180-x))/(double)y);
    a2 = atan(((double)(x))/(double)y);
    return a2+a1;
  }
  else
    return atan((double)x/(double)y);
}

double angle_two(double x, double y) { //angle between beacon 2 and 3
  double a1;
  double a2;
  if(x>180) {
    a1 = atan(((double)(x-180))/(double)y);
    a2 = atan(((double)(360-x))/(double)y);
    return a2+a1;
  }
  else if(x<180) {
    a1 = atan(((double)(360-x))/(double)y);
    a2 = atan(((double)(180-x))/(double)y);
    return a1-a2;
  }
  else
    return atan((double)x/(double)y);
}

int main() {
  double x,y,z; //z is a side of the triangle
  double tx,ty;
  double angle1,angle2,angleA,angleB,angleP;

  cout << "X: ";
  cin >> tx;
  cout << "Y: ";
  cin >> ty;

  cout << "Calc Angle 1: " << angle_one(tx,ty) * 180 / PI << "\nCalc Angle 2: " << angle_two(tx,ty) * 180 / PI << endl;

  angle1 = angle_one(tx,ty);
  angle2 = angle_two(tx,ty);

  angleA = atan( sin(PI - angle1 - angle2)/(sin(angle2)/sin(angle1) + cos(PI - angle1 - angle2)));

  if(angleA<0){
    angleA+=PI;
  }

  angleP = PI/2 - angleA;
  angleB = PI-angleA-angle1;

  z = BEACONDIST * sin(angleB) / sin(angle1); //180 is the distance between beacon 1 and 2

  x = sin(angleP) * z;
  y = sin(angleA) * z;



  cout << "X: " << x << "\nY: " << y << endl;

return 0;
}



