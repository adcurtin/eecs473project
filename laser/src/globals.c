//globals
extern char ReceivedChar;
extern int tx_count = 0;
extern int tx_buff_count = 0;
extern int tx_enable = 0;
extern int tx_buff_size = 0;
extern int rx_count = 0;
extern int add_to_buffer = 0;
extern int m_flag = 0;
extern int m_count = 0;
extern char * integer_buffer;
extern int radius = 10;  			//distance of one wheel to the center of the robot in inches
extern double pi = 3.141592653589;	//if you dont know what this is, then I don't know how you opened this file
extern int encoder_distance = 0;	//used to move the robot forward
extern int encoder_X = 0;			//used to change the angle of the robot
extern int encoder_Y = 0;			//used to change the angle of the robot
extern double orientation = 0; 		//orientation of the robot
extern double X = 0; 				//X position of the robot
extern double Y = 0;				//Y position of the robot
extern int PWM = 35;				//PWM of motor 2
int move_complete = 1;
int move_pos = 0;
int move_angle = 0;
unsigned char command_buffer[MAX_BUFFER_SIZE];
unsigned char transmit_buffer[MAX_BUFFER_SIZE];
unsigned char temp[MAX_BUFFER_SIZE];

struct command
{
	int type;
	int value;
};

