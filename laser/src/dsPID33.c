#include "dsPID33_definitions.h"
#include "math.h"
#include "string.h"

//#include "DEE Emulation 16-bit.h"

//Left motor is encoder 2 and PWM 2, POS2CNT
//Right motor is encoder 1 and PWM 1, POS1CNT
//must be hooked up this way on the PCB

#define BAUDRATE 9600
#define FCY 40000000
#define BRGVAL ((FCY/BAUDRATE)/16)-1
#define MAX_BUFFER_SIZE 50

//globals
volatile int enc_count = 0;
volatile int last_count = 0;
int beacons_read = 0;
int b1pos = 0;		// encoder values of beacons, in order that we read them
int b2pos = 0;
int b3pos = 0;
int enc_A = 0;		// encoder values of beacons A, B and C
int enc_B = 0;
int enc_C = 0;
int angle12 = 0;	// Encoder counts between beacons 1 and 2
int angle23 = 0;	// Encoder counts between beacons 2 and 3
int theta1_enc = 0;	// Encoder counts between beacons A and B
int theta2_enc = 0;	// Encoder counts between beacons B and C
int offset_enc = 0;
volatile double theta1 = 0;
volatile double theta2 = 0;
volatile int zero_offset;
double x, y, z; 				// position, z is a side of the triangle
double angleA, angleB, angleP;	// angles for calculating position
double orientation;				// angle we're facing

#define ENCRATIO (2*PI/MAX2CNT)	// Converts encoder counts to radians
#define ENCLIMIT (50)			// Threshold for noise

#define BEACONDIST (21)

/* UART */
char ReceivedChar;
int tx_count = 0;
int tx_buff_count = 0;
int tx_enable = 0;
int tx_buff_size = 0;
int rx_count = 0;
int add_to_buffer = 0;
int m_flag = 0;
int m_count = 0;
char * integer_buffer;
unsigned char command_buffer[MAX_BUFFER_SIZE];
unsigned char transmit_buffer[MAX_BUFFER_SIZE];
unsigned char temp[MAX_BUFFER_SIZE];

void init_UART()
{
//code taken from example 17-2 of http://www.asic.uwaterloo.ca/files/project/macgyver/Microchip_uart.pdf
	U1MODEbits.STSEL = 0; // 1 Stop bit
	U1MODEbits.PDSEL = 0; // No Parity, 8 data bits
	U1MODEbits.ABAUD = 0; // Auto-Baud Disabled
	U1MODEbits.BRGH = 0; // Low Speed mode
	U1BRG = BRGVAL; // BAUD Rate Setting for 9600
	U1STAbits.UTXISEL0 = 0; // Interrupt after one TX Character is transmitted
	U1STAbits.UTXISEL1 = 0;
	U1STAbits.URXISEL = 0; // Interrupt after one RX character is received;
	IEC0bits.U1TXIE = 1; // Enable UART TX Interrupt
	U1MODEbits.UARTEN = 1; // Enable UART
	U1STAbits.UTXEN = 1; // Enable UART TX


	U2MODEbits.STSEL = 0; // 1 Stop bit
	U2MODEbits.PDSEL = 0; // No Parity, 8 data bits
	U2MODEbits.ABAUD = 0; // Auto-Baud Disabled
	U2MODEbits.BRGH = 0; // Low Speed mode
	U2BRG = BRGVAL; // BAUD Rate Setting for 9600
	U2STAbits.UTXISEL0 = 0; // Interrupt after one TX Character is transmitted
	U2STAbits.UTXISEL1 = 0;
	U2STAbits.URXISEL = 0; // Interrupt after one RX character is received;
	//IEC0bits.U1TXIE = 1; // Enable UART TX Interrupt
	U2MODEbits.UARTEN = 1; // Enable UART
	U2STAbits.UTXEN = 1; // Enable UART TX
	/* wait at least 104 usec (1/9600) before sending first char */
	for (i = 0; i < 4160; i++)
		Nop();
}

void transmit_UART(unsigned char *message) {
	tx_buff_size = strlen((char *)message);
	memcpy(transmit_buffer, message, tx_buff_size);
	tx_enable = 1;
}

double angle_one(double x, double y) { //angle between beacon 1 and 2
	double a1, a2;

	if (x > 180) {
		a1 = atan(((double)(x-180))/(double)y);
		a2 = atan(((double)x)/(double)y);
		return (a2 - a1);
	} else if (x < 180) {
		a1 = atan(((double)(180-x))/(double)y);
		a2 = atan(((double)(x))/(double)y);
		return (a2 + a1);
	} else
		return (atan((double)x/(double)y));
}

double angle_two(double x, double y) { //angle between beacon 2 and 3
	double a1, a2;

	if (x > 180) {
		a1 = atan(((double)(x-180))/(double)y);
		a2 = atan(((double)(360-x))/(double)y);
		return (a2 + a1);
	} else if(x < 180) {
		a1 = atan(((double)(360-x))/(double)y);
		a2 = atan(((double)(180-x))/(double)y);
		return (a1 - a2);
	} else
		return (atan((double)x/(double)y));
}

void __attribute__((__interrupt__, no_auto_psv)) _INT1Interrupt(void) //0 degrees
{
	zero_offset = POS2CNT;
	LED2 = 1;

	while (abs(POS2CNT-zero_offset) < (MAX2CNT/4));

	IFS1bits.INT1IF = 0; // clear int1 interrupt flag
	IEC1bits.INT1IE = 0; // Disable interrupt 1
	IEC1bits.INT2IE = 1; // Enable interrupt 2
	LED2 = 0;
}

void __attribute__((__interrupt__, no_auto_psv)) _INT2Interrupt(void) //actual sensor
{
	enc_count = POS2CNT;

	unsigned char send[sizeof(double)*3 + 1 /* null */] = {0};

	/* WE SHOULD DISABLE ALL INTERUPTS WHEN WE ARE IN HERE */
	IEC1bits.INT2IE = 0;
	IFS1bits.INT2IF = 0; //clear interrupt flag

	if (abs(enc_count - last_count) < ENCLIMIT) { // probably noise. throw everything away.
		beacons_read = 0;
		b1pos = 0;
		b2pos = 0;
		b3pos = 0;
		goto done;
	}

	if (beacons_read == 0) {
		b1pos = enc_count;
		beacons_read++;
	} else if (beacons_read == 1) {
		if (enc_count < b1pos) {
			b1pos = enc_count;
			goto done;
		}
		b2pos = enc_count;
		beacons_read++;
	} else if (beacons_read == 2) {
		if (enc_count < b2pos) {
			b1pos = b2pos;
			b2pos = enc_count;
			goto done;
		}
		b3pos = enc_count;

		angle12 = (b1pos > b2pos) ? MAX2CNT - b1pos + b2pos : b2pos - b1pos;
		angle23 = (b2pos > b3pos) ? MAX2CNT - b2pos + b3pos : b3pos - b2pos;

		//figure out which is the large angle, and use the two small angles as the angles between the beacons
		if (angle12 > (MAX2CNT/2)) {
			// b2pos is beacon A
			enc_A = b2pos;
			enc_B = b3pos;
			enc_C = b1pos;
		} else if (angle23 > (MAX2CNT/2)) {
			// b3pos is beacon A
			enc_A = b3pos;
			enc_B = b1pos;
			enc_C = b2pos;
		} else {
			// b1pos is beacon A
			enc_A = b1pos;
			enc_B = b2pos;
			enc_C = b3pos;
		}

		theta1_enc = (enc_A > enc_B) ? MAX2CNT - enc_A + enc_B : enc_B - enc_A;
		theta2_enc = (enc_B > enc_C) ? MAX2CNT - enc_B + enc_C : enc_C - enc_B;
		theta1 = theta1_enc * ENCRATIO;	// Converts encoder counts to radians
		theta2 = theta2_enc * ENCRATIO;

		angleA = atan(sin(PI - theta1 - theta2)/(sin(theta2)/sin(theta1) + cos(PI - theta1 - theta2)));
		angleB = PI - theta1 - angleA;

		z = BEACONDIST * sin(angleB) / sin(theta1); // 180 is the distance between beacon 1 and 2
		x = cos(angleA) * z;
		y = sin(angleA) * z;

		/* encoder counts from zero to beacon A */
		offset_enc = (zero_offset > enc_A) ? MAX2CNT - zero_offset + enc_A : enc_A - zero_offset;

		orientation = PI/2 - angleA + offset_enc*ENCRATIO;

		beacons_read = 0;
		b1pos = 0;
		b2pos = 0;
		b3pos = 0;

		IEC1bits.INT2IE = 0;
		if (command_buffer[0] == 'p') {
			memcpy(&send[0*sizeof(double)], &x, sizeof(double));
			memcpy(&send[1*sizeof(double)], &y, sizeof(double));
			memcpy(&send[2*sizeof(double)], &orientation, sizeof(double));
			send[3*sizeof(double)] = '\0';
			transmit_UART(send);
#if 0
			char buf[100] = {0};		// uncomment this to print x y orientation to serial
			snprintf(buf, 100, "%g %g %g\n", x, y, orientation);
			transmit_UART(buf);
			rx_count = 0;
			memset(command_buffer, '\0', sizeof(command_buffer));
#endif
		}
		IEC1bits.INT2IE = 1;
	}

	last_count = enc_count;

done:
	IEC1bits.INT2IE = 1;
}

int main (void)
{
	Settings();
	init_UART();		//init the UART
	ADPCFG = 0xFFFF;	//make ADC pins all digital
	LED1 = 1; //LED on when program running
	//transmit_UART("Initializing Laser");
    for (;;) {
		if(tx_count < 55)
			tx_count++;
	
		if(tx_count >= 55 && tx_enable) {
			U1TXREG = transmit_buffer[tx_buff_count];
			tx_buff_count++;	
	
			//if we've transmitted all of the data, stop transmitting 
			if(tx_buff_count >= tx_buff_size) {
				tx_enable = 0;
				tx_buff_count = 0;
			}
		}


		/* check for receive errors */
		if(U1STAbits.FERR == 1)
		{
			continue;
		}
		/* must clear the overrun error to keep uart receiving */
		if(U1STAbits.OERR == 1)
		{
			U1STAbits.OERR == 0;
			continue;
		}
	
		/* get the data */
		if(U1STAbits.URXDA == 1)
		{
			ReceivedChar = U1RXREG;
			add_to_buffer = 1;
		}
	
		if(ReceivedChar == '@')
		{
			add_to_buffer =  1;
			rx_count = 0;
			memset(command_buffer, '\0', MAX_BUFFER_SIZE);
		}
	
		if(add_to_buffer)
		{
			command_buffer[rx_count] = ReceivedChar;
			rx_count++;
			add_to_buffer = 0;
		}

		for (i = 0; i < 500; ++i)
			Nop();
	}
	return (0);
}

/**********************************************************************************
	UART Interrupts 
***********************************************************************************/

void __attribute__((__interrupt__, no_auto_psv)) _U1TXInterrupt(void)
{
	IFS0bits.U1TXIF = 0; // clear TX interrupt flag
	tx_count = 0;        //have to wait 55 clock cycles between transfers
}

void _ISR_PSV _U1RXInterrupt(void)
{
}
